import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class MaximumSubarray {
    static class Solution {
        //动态规划
        public int maxSubArray(int[] nums) {
            int pre = 0, maxAns = nums[0];
            for (int x : nums) {
                pre = Math.max(pre + x, x);
                maxAns = Math.max(maxAns, pre);
            }
            return maxAns;
        }
        //打印子数组
        public int printMaxSubArray(int[] nums){
            int ans = nums[0];
            // dp[i] = 以nums[i]结尾的子序列的最大和
            int[] dp = new int[nums.length];
            dp[0] = nums[0];
            int maxStart = 0, maxLen = 1;// 记录最大连续子序列的起点和长度
            int start =0, len = 1; // 记录连续子序列的起点和长度
            for(int i=1; i<nums.length; ++i){
                // dp[i] = Math.max(dp[i-1] + nums[i], nums[i]); // 是继续在后面添加，还是另起一个新序列
                if(dp[i-1] + nums[i] > nums[i]){ // 后面接上当前元素
                    dp[i] = dp[i-1] + nums[i];
                    len++;
                }else{ // 另起一个新序列
                    dp[i] = nums[i];
                    start = i;
                    len = 1;
                }
                if(dp[i] > ans){
                    maxStart = start;
                    maxLen = len;
                    ans = dp[i];
                }
            }
            System.out.println(maxLen);
            System.out.println(Arrays.toString(Arrays.copyOfRange(nums, maxStart, maxStart+maxLen)));
            return ans;
        }
    }


        public static int[] stringToIntegerArray(String input) {
            input = input.trim();
            input = input.substring(1, input.length() - 1);
            if (input.length() == 0) {
                return new int[0];
            }

            String[] parts = input.split(",");
            int[] output = new int[parts.length];
            for(int index = 0; index < parts.length; index++) {
                String part = parts[index].trim();
                output[index] = Integer.parseInt(part);
            }
            return output;
        }

        public static void main(String[] args) throws IOException {
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            String line;
            while ((line = in.readLine()) != null) {
                int[] nums = stringToIntegerArray(line);

                int ret = new Solution().printMaxSubArray(nums);

                String out = String.valueOf(ret);

                System.out.print(out);
            }
        }

}
