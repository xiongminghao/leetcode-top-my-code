import DataStructure.ListNode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;

public class IntersectionofTwoLinkedLists {
    static class Solution{
        //哈希集合判断是否经过另一个链表的结点
        public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
            if(headA==null||headB==null)
                return null;
            ListNode a=headA;
            ListNode b=headB;
            HashSet<ListNode> set=new HashSet<>();
            while(a!=null)
            {
                set.add(a);
                a=a.next;
            }
            while(b!=null)
            {
                if(set.contains(b))
                    return b;
                b=b.next;
            }
            return null;

        }
        //时间复杂度O(m+n)
        //空间复杂度O(m)

        //双指针，走对方的路
        public ListNode getIntersectionNode2(ListNode headA,ListNode headB){
                if(headA==null||headB==null)
                    return null;
                ListNode a=headA;
                ListNode b=headB;
                while(a!=b)
                {
                    a=a==null?headB:a.next;
                    b=b==null?headA:b.next;
                }
                return a;
        }
        //时间复杂度O(m+n)
        //空间复杂度O(1)

    }
    public static int[] stringToIntegerArray(String input) {
        input = input.trim();
        input = input.substring(1, input.length() - 1);
        if (input.length() == 0) {
            return new int[0];
        }

        String[] parts = input.split(",");
        int[] output = new int[parts.length];
        for(int index = 0; index < parts.length; index++) {
            String part = parts[index].trim();
            output[index] = Integer.parseInt(part);
        }
        return output;
    }

    public static ListNode stringToListNode(String input) {
        // Generate array from the input
        int[] nodeValues = stringToIntegerArray(input);

        // Now convert that list into linked list
        ListNode dummyRoot = new ListNode(0);
        ListNode ptr = dummyRoot;
        for(int item : nodeValues) {
            ptr.next = new ListNode(item);
            ptr = ptr.next;
        }
        return dummyRoot.next;
    }

    public static String listNodeToString(ListNode node) {
        if (node == null) {
            return "[]";
        }

        String result = "";
        while (node != null) {
            result += Integer.toString(node.val) + ", ";
            node = node.next;
        }
        return "[" + result.substring(0, result.length() - 2) + "]";
    }

    public static void main(String[] args) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String line;
        while ((line = in.readLine()) != null) {
            int intersectVal = Integer.parseInt(line);
            line = in.readLine();
            ListNode listA = stringToListNode(line);
            line = in.readLine();
            ListNode listB = stringToListNode(line);
            line = in.readLine();
            int skipA = Integer.parseInt(line);
            line = in.readLine();
            int skipB = Integer.parseInt(line);

            ListNode ret = new Solution().getIntersectionNode(listA, listB);

            String out = listNodeToString(ret);

            System.out.print(out);
        }
    }
}
