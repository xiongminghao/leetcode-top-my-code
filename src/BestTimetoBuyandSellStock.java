import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class BestTimetoBuyandSellStock {
    static class Solution{
        //维护一个最小值
        public int maxProfit(int[] prices) {
            if(prices.length <= 1)
                return 0;
            int min = prices[0], max = 0;
            for(int i = 1; i < prices.length; i++) {
                max = Math.max(max, prices[i] - min);
                min = Math.min(min, prices[i]);
            }
            return max;
        }
        //时间复杂度O(n)
        //空间复杂度O(1)
    }

    public static int[] stringToIntegerArray(String input) {
        input = input.trim();
        input = input.substring(1, input.length() - 1);
        if (input.length() == 0) {
            return new int[0];
        }

        String[] parts = input.split(",");
        int[] output = new int[parts.length];
        for(int index = 0; index < parts.length; index++) {
            String part = parts[index].trim();
            output[index] = Integer.parseInt(part);
        }
        return output;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String line;
        while ((line = in.readLine()) != null) {
            int[] prices = stringToIntegerArray(line);

            int ret = new Solution().maxProfit(prices);

            String out = String.valueOf(ret);

            System.out.print(out);
        }
    }
}
