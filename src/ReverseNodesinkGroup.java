import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.Deque;

import DataStructure.ListNode;
public class ReverseNodesinkGroup {
    static class Solution{
        //简单易懂的解法
        public ListNode reverseKGroup(ListNode head, int k) {
            ListNode dummy = new ListNode(-1);
            dummy.next = head;
            ListNode pre = dummy;
            ListNode cur = head;
            while (cur != null) {
                // 分段的头结点cur--翻转后将变成尾节点
                // 分段的尾节点
                ListNode tailTmp = cur;
                int index = 1;
                while (tailTmp.next != null && index++ < k) {
                    tailTmp = tailTmp.next;
                }
                // 若个数不足k，则无须翻转，而且已至尾部，直接返回
                // 若不足k个也要反转，忽略这个条件即可

                if (index < k) {
                    pre.next = cur;
                    return dummy.next;
                }
                // 满足k个，保存下个分组的头结点，再断开当前分组，调用翻转
                ListNode curTmp = tailTmp.next;
                tailTmp.next = null;
                pre.next = reverseList(cur);
                // 进入下组
                pre = cur;
                cur = curTmp;
            }

            return dummy.next;
        }

        public ListNode reverseList(ListNode head) {
            ListNode pre = null;
            while (head != null) {
                ListNode tmp = head.next;
                head.next = pre;
                pre = head;
                head = tmp;
            }
            return pre;
        }

    }
    public static int[] stringToIntegerArray(String input) {
        input = input.trim();
        input = input.substring(1, input.length() - 1);
        if (input.length() == 0) {
            return new int[0];
        }

        String[] parts = input.split(",");
        int[] output = new int[parts.length];
        for(int index = 0; index < parts.length; index++) {
            String part = parts[index].trim();
            output[index] = Integer.parseInt(part);
        }
        return output;
    }

    public static ListNode stringToListNode(String input) {
        // Generate array from the input
        int[] nodeValues = stringToIntegerArray(input);

        // Now convert that list into linked list
        ListNode dummyRoot = new ListNode(0);
        ListNode ptr = dummyRoot;
        for(int item : nodeValues) {
            ptr.next = new ListNode(item);
            ptr = ptr.next;
        }
        return dummyRoot.next;
    }

    public static String listNodeToString(ListNode node) {
        if (node == null) {
            return "[]";
        }

        String result = "";
        while (node != null) {
            result += Integer.toString(node.val) + ", ";
            node = node.next;
        }
        return "[" + result.substring(0, result.length() - 2) + "]";
    }

    public static void main(String[] args) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String line;
        while ((line = in.readLine()) != null) {
            ListNode head = stringToListNode(line);
            line = in.readLine();
            int k = Integer.parseInt(line);

            ListNode ret = new Solution().reverseKGroup(head, k);

            String out = listNodeToString(ret);

            System.out.print(out);
        }
    }
}
