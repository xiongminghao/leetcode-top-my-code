import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class KthLargestElementinanArray {
    static class Solution {
        public int findKthLargest(int[] nums, int k) {
            heapSort(nums);
            return nums[nums.length-k];
        }
        //快排
        public void quickSort(int[] arr,int l,int r){
            if(l<r){
                int pivotpos = partition(arr,l,r);
                quickSort(arr,l,pivotpos-1);
                quickSort(arr,pivotpos+1,r);
            }
        }
        //优化，基于快排的选择
        public int quickSelect(int[] arr,int l,int r,int loc){
                int pivotpos = partition(arr,l,r);//pivotpos的位置已经确定了，跟第k大的下标比较一下
                if(pivotpos==loc)
                    return arr[pivotpos];
                else
                    return pivotpos<loc?quickSelect(arr,pivotpos+1,r,loc):quickSelect(arr,l,pivotpos-1,loc);
        }
        public int partition(int[] arr,int l,int r){
            int pivot = arr[l];
            while (l<r){
                while(l<r&&arr[r]>=pivot)
                    r--;
                arr[l]=arr[r];
                while(l<r&&arr[l]<=pivot)
                    l++;
                arr[r]=arr[l];
            }
            arr[l]=pivot;
            return l;
        }
        //时间复杂度O(n)
        //空间复杂度O(logn)

        //堆排序
        public void buildMaxHeap(int[] arr,int len){
            for(int i=len/2;i>=0;i--)
                maxHeapify(arr,len,i);
            for(int i=len;i>=1;i--){
                swap(arr,i,0);
                len--;
                maxHeapify(arr,len,0);
            }
        }
        public void maxHeapify(int[] arr,int len,int index){
            while(index*2+1<=len){
                int l=index*2+1;
                int r=index*2+2;
                int large=index;
                if(l<=len&&arr[l]>arr[large])
                    large=l;
                if(r<=len&&arr[r]>arr[large])
                    large=r;
                if(large!=index){
                    swap(arr,index,large);
                    index=large;
                }
                else
                    break;
            }
        }
        public void heapSort(int[] arr){
            int len=arr.length-1;
            buildMaxHeap(arr,len);
        }
        public void swap(int[] arr,int i,int j){
            int temp=arr[i];
            arr[i]=arr[j];
            arr[j]=temp;
        }
    }
    public static int[] stringToIntegerArray(String input) {
        input = input.trim();
        input = input.substring(1, input.length() - 1);
        if (input.length() == 0) {
            return new int[0];
        }

        String[] parts = input.split(",");
        int[] output = new int[parts.length];
        for(int index = 0; index < parts.length; index++) {
            String part = parts[index].trim();
            output[index] = Integer.parseInt(part);
        }
        return output;
    }
    public static void main(String[] args) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String line;
        while ((line = in.readLine()) != null) {
            int[] nums = stringToIntegerArray(line);
            line = in.readLine();
            int k = Integer.parseInt(line);
            int ret = new Solution().findKthLargest(nums, k);
            String out = String.valueOf(ret);
            System.out.print(out);
        }
    }

}
