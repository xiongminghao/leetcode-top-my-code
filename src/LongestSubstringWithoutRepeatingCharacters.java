import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class LongestSubstringWithoutRepeatingCharacters {
    static class Solution {
        //经典左右指针滑动窗口，左右指针各遍历一次
        public int lengthOfLongestSubstring(String s) {
            Map<Character,Integer> map=new HashMap<>();//用数组也可
            int max=-1;
            int left=0;
            for(int right=0;right<s.length();right++){
                map.put(s.charAt(right),map.getOrDefault(s.charAt(right),0)+1);
                while(left<=right&&map.get(s.charAt(right))>1){
                    map.put(s.charAt(left),map.getOrDefault(s.charAt(left),0)-1);
                    if(map.get(s.charAt(left))==0)
                        map.remove(s.charAt(left));
                    left++;
                }
                max=Math.max(max,right-left+1);
            }
            return max==-1?0:max;
        }
        //输出最长子串
        public int lengthOfLongestSubstring2(String s) {
            Map<Character,Integer> map=new HashMap<>();//用数组也可
            int ll=0,rr=0;
            int max=-1;
            int left=0;
            for(int right=0;right<s.length();right++){
                map.put(s.charAt(right),map.getOrDefault(s.charAt(right),0)+1);
                while(left<=right&&map.get(s.charAt(right))>1){
                    map.put(s.charAt(left),map.getOrDefault(s.charAt(left),0)-1);
                    if(map.get(s.charAt(left))==0)
                        map.remove(s.charAt(left));
                    left++;
                }
                if(right-left+1>max){
                    max=right-left+1;
                    ll=left;
                    rr=right;
                }

            }
            System.out.println(s.substring(ll,rr+1));
            return max==-1?0:max;
        }
        //只遍历一次的做法
        public int lengthOfLongestSubstring3(String s) {
            // 记录字符上一次出现的位置
            int[] last = new int[128];
            for(int i = 0; i < 128; i++) {
                last[i] = -1;
            }
            int n = s.length();
            int res = 0;
            int start = 0; // 窗口开始位置
            for(int i = 0; i < n; i++) {
                int index = s.charAt(i);
                start = Math.max(start, last[index] + 1);
                res   = Math.max(res, i - start + 1);
                last[index] = i;
            }
            return res;
        }
    }

    public static void main(String[] args) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String line;
        while ((line = in.readLine()) != null) {
            String s = line;
            int ret = new Solution().lengthOfLongestSubstring2(s);
            String out = String.valueOf(ret);
            System.out.print(out);
        }
    }
}
