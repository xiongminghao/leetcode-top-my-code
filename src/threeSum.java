import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class threeSum {
    static class Solution{
        public List<List<Integer>> threeSum(int[] nums) {
            List<List<Integer>> lists=new ArrayList<>();
            Arrays.sort(nums);
            for(int k=0;k<nums.length-2;k++){
                if(k>0&&nums[k]==nums[k-1])
                    continue;
                int left=k+1;
                int right=nums.length-1;
                while(left<right){
                    int sum=nums[k]+nums[left]+nums[right];
                    if(sum<0){
                        left++;
                    }
                    else if(sum>0){
                        right--;
                    }
                    else{

                        lists.add(Arrays.asList(nums[k],nums[left],nums[right]));
                        while(left<right&&nums[left]==nums[left+1])
                            left++;
                        while(left<right&&nums[right]==nums[right-1])
                            right--;
                        left++;
                        right--;
                    }
                }
            }
            return lists;
        }
    }
    public static int[] stringToIntegerArray(String input) {
        input = input.trim();
        input = input.substring(1, input.length() - 1);
        if (input.length() == 0) {
            return new int[0];
        }

        String[] parts = input.split(",");
        int[] output = new int[parts.length];
        for(int index = 0; index < parts.length; index++) {
            String part = parts[index].trim();
            output[index] = Integer.parseInt(part);
        }
        return output;
    }

    public static String integerArrayListToString(List<Integer> nums, int length) {
        if (length == 0) {
            return "[]";
        }

        String result = "";
        for(int index = 0; index < length; index++) {
            Integer number = nums.get(index);
            result += Integer.toString(number) + ", ";
        }
        return "[" + result.substring(0, result.length() - 2) + "]";
    }

    public static String integerArrayListToString(List<Integer> nums) {
        return integerArrayListToString(nums, nums.size());
    }

    public static String int2dListToString(List<List<Integer>> nums) {
        StringBuilder sb = new StringBuilder("[");
        for (List<Integer> list: nums) {
            sb.append(integerArrayListToString(list));
            sb.append(",");
        }

        sb.setCharAt(sb.length() - 1, ']');
        return sb.toString();
    }

    public static void main(String[] args) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String line;
        while ((line = in.readLine()) != null) {
            int[] nums = stringToIntegerArray(line);

            List<List<Integer>> ret = new Solution().threeSum(nums);

            String out = int2dListToString(ret);

            System.out.print(out);
        }
    }
}
