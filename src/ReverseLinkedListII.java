import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import DataStructure.ListNode;

public class ReverseLinkedListII {
    static class Solution{
        //常规方法，找到区间进行反转
        public ListNode reverseBetween(ListNode head, int m, int n){
            ListNode dummy=new ListNode(-1,head);
            ListNode start=dummy;
            for(int i=1;i<m;i++)
                start=start.next;
            ListNode pre=null;
            ListNode cur=start.next;
            for(int i=0;i<=n-m;i++){
                ListNode temp=cur.next;
                cur.next=pre;
                pre=cur;
                cur=temp;
            }
            start.next.next=cur;
            start.next=pre;
            return dummy.next;
        }
        //头插法实现反转
        public ListNode reverseBetween2(ListNode head, int m, int n) {
            ListNode dummyNode = new ListNode(-1);
            dummyNode.next = head;
            ListNode pre = dummyNode;
            for (int i = 0; i < m - 1; i++) {
                pre = pre.next;
            }
            ListNode cur = pre.next;
            ListNode next;
            for (int i = 0; i < n-m; i++) {
                next = cur.next;
                cur.next = next.next;
                next.next = pre.next;
                pre.next = next;
            }
            return dummyNode.next;
        }
    }

    public static int[] stringToIntegerArray(String input) {
        input = input.trim();
        input = input.substring(1, input.length() - 1);
        if (input.length() == 0) {
            return new int[0];
        }

        String[] parts = input.split(",");
        int[] output = new int[parts.length];
        for(int index = 0; index < parts.length; index++) {
            String part = parts[index].trim();
            output[index] = Integer.parseInt(part);
        }
        return output;
    }

    public static ListNode stringToListNode(String input) {
        // Generate array from the input
        int[] nodeValues = stringToIntegerArray(input);

        // Now convert that list into linked list
        ListNode dummyRoot = new ListNode(0);
        ListNode ptr = dummyRoot;
        for(int item : nodeValues) {
            ptr.next = new ListNode(item);
            ptr = ptr.next;
        }
        return dummyRoot.next;
    }

    public static String listNodeToString(ListNode node) {
        if (node == null) {
            return "[]";
        }

        String result = "";
        while (node != null) {
            result += Integer.toString(node.val) + ", ";
            node = node.next;
        }
        return "[" + result.substring(0, result.length() - 2) + "]";
    }

    public static void main(String[] args) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String line;
        while ((line = in.readLine()) != null) {
            ListNode head = stringToListNode(line);
            line = in.readLine();
            int left = Integer.parseInt(line);
            line = in.readLine();
            int right = Integer.parseInt(line);
            ListNode ret = new Solution().reverseBetween(head, left, right);
            String out = listNodeToString(ret);
            System.out.println(out);
        }
    }
}
