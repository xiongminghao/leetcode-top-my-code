import DataStructure.TreeNode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BinaryTreeLevelOrderTraversal {
    static class Solution{
        //迭代
        public List<List<Integer>> levelOrder(TreeNode root) {
            if(root == null)
                return new ArrayList<>();
            List<List<Integer>> res = new ArrayList<>();
            Queue<TreeNode> queue = new LinkedList<TreeNode>();
            queue.add(root);
            while(!queue.isEmpty()){
                int count = queue.size();
                List<Integer> list = new ArrayList<Integer>();
                while(count > 0){
                    TreeNode node = queue.poll();
                    list.add(node.val);
                    if(node.left != null)
                        queue.add(node.left);
                    if(node.right != null)
                        queue.add(node.right);
                    count--;
                }
                res.add(list);
            }
            return res;
        }
        //时间复杂度O(n)
        //空间复杂度O(n)

        //递归
        public List<List<Integer>> levelOrder2(TreeNode root) {
            if(root==null) {
                return new ArrayList<List<Integer>>();
            }
            //用来存放最终结果
            List<List<Integer>> res = new ArrayList<List<Integer>>();
            dfs(1,root,res);
            return res;
        }

        void dfs(int index,TreeNode root, List<List<Integer>> res) {
            //假设res是[ [1],[2,3] ]， index是3，就再插入一个空list放到res中
            if(res.size()<index) {
                res.add(new ArrayList<Integer>());
            }
            //将当前节点的值加入到res中，index代表当前层，假设index是3，节点值是99
            //res是[ [1],[2,3] [4] ]，加入后res就变为 [ [1],[2,3] [4,99] ]
            res.get(index-1).add(root.val);
            //递归的处理左子树，右子树，同时将层数index+1
            if(root.left!=null) {
                dfs(index+1, root.left, res);
            }
            if(root.right!=null) {
                dfs(index+1, root.right, res);
            }
        }
        //时间复杂度O(n)
        //空间复杂度O(h)

    }
    public static TreeNode stringToTreeNode(String input) {
        input = input.trim();
        input = input.substring(1, input.length() - 1);
        if (input.length() == 0) {
            return null;
        }

        String[] parts = input.split(",");
        String item = parts[0];
        TreeNode root = new TreeNode(Integer.parseInt(item));
        Queue<TreeNode> nodeQueue = new LinkedList<>();
        nodeQueue.add(root);

        int index = 1;
        while(!nodeQueue.isEmpty()) {
            TreeNode node = nodeQueue.remove();

            if (index == parts.length) {
                break;
            }

            item = parts[index++];
            item = item.trim();
            if (!item.equals("null")) {
                int leftNumber = Integer.parseInt(item);
                node.left = new TreeNode(leftNumber);
                nodeQueue.add(node.left);
            }

            if (index == parts.length) {
                break;
            }

            item = parts[index++];
            item = item.trim();
            if (!item.equals("null")) {
                int rightNumber = Integer.parseInt(item);
                node.right = new TreeNode(rightNumber);
                nodeQueue.add(node.right);
            }
        }
        return root;
    }

    public static String integerArrayListToString(List<Integer> nums, int length) {
        if (length == 0) {
            return "[]";
        }

        String result = "";
        for(int index = 0; index < length; index++) {
            Integer number = nums.get(index);
            result += Integer.toString(number) + ", ";
        }
        return "[" + result.substring(0, result.length() - 2) + "]";
    }

    public static String integerArrayListToString(List<Integer> nums) {
        return integerArrayListToString(nums, nums.size());
    }

    public static String int2dListToString(List<List<Integer>> nums) {
        StringBuilder sb = new StringBuilder("[");
        for (List<Integer> list: nums) {
            sb.append(integerArrayListToString(list));
            sb.append(",");
        }

        sb.setCharAt(sb.length() - 1, ']');
        return sb.toString();
    }

    public static void main(String[] args) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String line;
        while ((line = in.readLine()) != null) {
            TreeNode root = stringToTreeNode(line);

            List<List<Integer>> ret = new Solution().levelOrder(root);

            String out = int2dListToString(ret);

            System.out.print(out);
        }
    }
}
